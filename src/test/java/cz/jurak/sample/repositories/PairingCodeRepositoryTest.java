package cz.jurak.sample.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.PairingCode;
import cz.jurak.sample.models.dao.SubjectGroup;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PairingCodeRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private PairingCodeRepository pairingCodeRepository;

  @Test
  public void findAllTest() throws Exception {
    Iterable<PairingCode> pairingCodeIterable = this.pairingCodeRepository.findAll();
    for (PairingCode pairingCode : pairingCodeIterable) {
      assertTrue(pairingCode != null);
    }
  }

  @Test
  public void findExpiredPairingCodes() throws Exception {
    SubjectGroup subjectGroup = this.entityManager.persist(
        SubjectGroup.builder()
            .name("test")
            .active(true)
            .test(true)
            .created(Timestamp.valueOf(LocalDateTime.now().minusMinutes(5)))
            .modified(Timestamp.valueOf(LocalDateTime.now().minusMinutes(5)))
            .build()
    );
    this.entityManager.persist(
        PairingCode.builder()
            .subjectGroup(subjectGroup)
            .code("1234")
            .expireAt(Timestamp.valueOf(LocalDateTime.now().minusMinutes(5)))
            .build()
    );
    this.entityManager.persist(
        PairingCode.builder()
            .subjectGroup(subjectGroup)
            .code("4321")
            .expireAt(Timestamp.valueOf(LocalDateTime.now().plusMinutes(5)))
            .build()
    );
    LocalDateTime now = LocalDateTime.now();
    List<PairingCode> pairingCodes = this.pairingCodeRepository.findByExpireAtBefore(Timestamp.valueOf(now));
    pairingCodes.forEach(p -> assertThat(p.getExpireAt()).isBefore(Timestamp.valueOf(now)));
  }
}