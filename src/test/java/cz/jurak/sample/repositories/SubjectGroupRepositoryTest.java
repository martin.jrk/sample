package cz.jurak.sample.repositories;

import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.SubjectGroup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SubjectGroupRepositoryTest {

  @Autowired
  private SubjectGroupRepository subjectGroupRepository;

  @Test
  public void findAllTest() throws Exception {
    Iterable<SubjectGroup> subjectGroupIterable = this.subjectGroupRepository.findAll();
    for (SubjectGroup subjectGroup : subjectGroupIterable) {
      assertTrue(subjectGroup != null);
    }
  }
}