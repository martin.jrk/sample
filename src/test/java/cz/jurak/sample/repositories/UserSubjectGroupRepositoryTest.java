package cz.jurak.sample.repositories;

import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.UserSubjectGroup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserSubjectGroupRepositoryTest {

  @Autowired
  private UserSubjectGroupRepository userSubjectGroupRepository;

  @Test
  public void findAllTest() throws Exception {
    Iterable<UserSubjectGroup> userSubjectGroupIterable = this.userSubjectGroupRepository.findAll();
    for (UserSubjectGroup userSubjectGroup : userSubjectGroupIterable) {
      assertTrue(userSubjectGroup != null);
    }
  }
}