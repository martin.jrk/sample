package cz.jurak.sample.repositories;

import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.Subject;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SubjectRepositoryTest {

  @Autowired
  private SubjectRepository subjectRepository;

  @Autowired
  private EntityManager entityManager;

  @Test
  public void findAllTest() throws Exception {
    Iterable<Subject> subjectIterable = this.subjectRepository.findAll();
    for (Subject subject : subjectIterable) {
      assertTrue(subject != null);
    }
  }
}