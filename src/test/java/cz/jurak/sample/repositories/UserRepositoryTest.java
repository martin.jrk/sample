package cz.jurak.sample.repositories;

import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

  @Autowired
  private UserRepository userRepository;

  @Test
  public void findAllTest() throws Exception {
    Iterable<User> userIterable = this.userRepository.findAll();
    for (User user : userIterable) {
      assertTrue(user != null);
    }
  }
}