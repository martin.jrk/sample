package cz.jurak.sample.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CodeGeneratorTest {

  @Test
  public void generateCode() {
    String code = CodeGenerator.generateCode();
    assertTrue(code.length() == 4);
  }

  @Test
  public void formatThreeDigitNumber() {
    int num = 333;
    String numStr = CodeGenerator.formatNumber(333);
    assertThat(numStr).isEqualTo("0333");
  }
}