package cz.jurak.sample.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.PairingCode;
import cz.jurak.sample.models.responses.GroupResponse;
import cz.jurak.sample.models.responses.PairingResponse;
import cz.jurak.sample.repositories.PairingCodeRepository;
import cz.jurak.sample.repositories.SubjectGroupRepository;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PairingCodeServiceTest {

  @Autowired
  private PairingCodeService pairingCodeService;

  @Autowired
  private GroupService groupService;

  @Autowired
  private PairingCodeRepository pairingCodeRepository;

  @Test
  public void generateCode() throws Exception {
    PairingResponse pairingResponse = this.pairingCodeService.generatePairingCode(null, true);
    assertTrue(pairingResponse != null);
    assertTrue(pairingResponse.getCode().length() == 4);
    assertTrue(pairingResponse.getGroupId() != null);

    GroupResponse groupResponse = groupService.getGroup(pairingResponse.getGroupId());
    assertThat(groupResponse.getName()).isEqualTo("default");
  }

  @Test
  public void generateCodeWithGroupAndExistingCode() throws Exception {
    PairingResponse newPairingResponse = this.pairingCodeService.generatePairingCode(null, true);
    PairingResponse pairingResponse = this.pairingCodeService
        .generatePairingCode(newPairingResponse.getGroupId(), false);
    assertThat(newPairingResponse.getCode()).isNotEqualTo(pairingResponse.getCode());
    assertThat(newPairingResponse.getGroupId()).isEqualTo(pairingResponse.getGroupId());
  }

  @Test
  public void generateCodeWithGroupAndNewCode() throws Exception {
    PairingResponse newPairingResponse = this.pairingCodeService.generatePairingCode(null, true);
    this.pairingCodeRepository.delete(this.pairingCodeRepository.findByCode(newPairingResponse.getCode()).get());
    PairingResponse pairingResponse = this.pairingCodeService
        .generatePairingCode(newPairingResponse.getGroupId(), false);
    assertThat(newPairingResponse.getCode()).isNotEqualTo(pairingResponse.getCode());
  }

  @Test
  public void getExistingPairingCode() throws Exception {
    PairingResponse pairingResponse = this.pairingCodeService.generatePairingCode(null, true);
    PairingResponse pairingResponseConfirm = this.pairingCodeService.confirmPairingCode(pairingResponse.getCode());
    assertThat(pairingResponseConfirm.getCode()).isEqualTo(pairingResponse.getCode());
    assertThat(pairingResponseConfirm.getGroupId()).isEqualTo(pairingResponse.getGroupId());
    assertThat(pairingResponseConfirm.getMqttPassword()).isEqualTo(pairingResponse.getMqttPassword());
    assertThat(pairingResponseConfirm.getMqttUsername()).isEqualTo(pairingResponse.getMqttUsername());
  }


  @Test(expected = Exception.class)
  public void getUnknownPairingCode() throws Exception {
    PairingResponse pairingResponse = this.pairingCodeService.generatePairingCode(null, true);
    String lastNumber = pairingResponse.getCode()
        .substring(pairingResponse.getCode().length() - 1, pairingResponse.getCode().length());
    Integer code = Integer.valueOf(lastNumber) + 1;
    this.pairingCodeService.confirmPairingCode(String.valueOf(code));
  }

  @Test
  public void deletePairingCodeTest() throws Exception {
    PairingResponse pairingResponse = this.pairingCodeService.generatePairingCode(null, true);
    this.pairingCodeService.deletePairingCode(pairingResponse.getCode());

    Optional<PairingCode> pairingCodeOptional = this.pairingCodeRepository.findByCode(pairingResponse.getCode());
    assertThat(pairingCodeOptional.isPresent()).isFalse();
  }

  @Test
  public void deleteUnknownPairingCodeTest() {
    this.pairingCodeService.deletePairingCode("0");
  }
}