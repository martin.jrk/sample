package cz.jurak.sample.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.User;
import cz.jurak.sample.models.responses.UserResponse;
import cz.jurak.sample.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserService userService;

  private static final String EMAIL = "test@test.com";
  private static final String PASSWORD = "password";

  @Test
  public void getUserByEmailAndMqttPasswordTest() throws Exception {
    User user = User.builder().email(EMAIL).mqttPassword(PASSWORD).build();
    userRepository.save(user);

    UserResponse userResponse = userService.getUserByEmailAndMqttPassword(EMAIL, PASSWORD);

    assertThat(userResponse.getEmail()).isEqualTo(EMAIL);
    assertThat(userResponse.getName()).isEqualTo(EMAIL);
  }

  @Test(expected = NotFoundException.class)
  public void getUserByEmailAndMqttPasswordNoFoundTest() throws Exception {
    userService.getUserByEmailAndMqttPassword("testNotFound@test.com", "passwordnotfound");
  }

  @Test
  public void createTrialUserOk() {
    User trialUser = userService.createTrialUser();
    assertThat(trialUser.getEmail())
        .containsPattern("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}@trial-user.trial");
    assertThat(trialUser.getMqttPassword()).containsPattern("[0-9a-zA-Z]{32}");
    assertTrue(trialUser.isTrial());
  }
}