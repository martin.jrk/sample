package cz.jurak.sample.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.dao.SubjectGroup;
import cz.jurak.sample.models.dao.User;
import cz.jurak.sample.models.dao.UserSubjectGroup;
import cz.jurak.sample.models.requests.GroupRequest;
import cz.jurak.sample.models.responses.GroupResponse;
import cz.jurak.sample.models.responses.PairingResponse;
import cz.jurak.sample.repositories.SubjectGroupRepository;
import cz.jurak.sample.repositories.SubjectRepository;
import cz.jurak.sample.repositories.UserRepository;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GroupServiceTest {

  @Autowired
  private GroupService groupService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SubjectGroupRepository subjectGroupRepository;

  @Autowired
  private SubjectRepository subjectRepository;

  @Autowired
  private PairingCodeService pairingCodeService;

  private static final String TEST_GROUP_NAME = "group";

  @Test
  public void createGroupTestOK() throws Exception {
    GroupRequest groupRequest = new GroupRequest(TEST_GROUP_NAME, false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    assertTrue(groupResponse != null);
    assertTrue(groupRequest.getName().equals(groupResponse.getName()));
    assertTrue(groupRequest.getTest() == groupResponse.isTest());
    assertTrue(groupResponse.getId() != null);
  }

  @Test
  public void getGroupTestExist() throws Exception {
    GroupRequest groupRequest = new GroupRequest(TEST_GROUP_NAME, false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    groupResponse = this.groupService.getGroup(groupResponse.getId());
    assertTrue(groupResponse != null);
    assertTrue(groupRequest.getName().equals(groupResponse.getName()));
    assertTrue(groupRequest.getTest() == groupResponse.isTest());
    assertTrue(groupResponse.getId() != null);
  }

  @Test(expected = NotFoundException.class)
  public void getGroupNotExistTest() throws Exception {
    GroupRequest groupRequest = new GroupRequest(TEST_GROUP_NAME, false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    this.groupService.getGroup(groupResponse.getId() + 1);
  }

  @Test
  public void addUserToGroupOk() {
    User user = this.userRepository
        .save(User.builder().email("email@email.com").mqttPassword("123456")
            .active(true)
            .created(Timestamp.valueOf(LocalDateTime.now()))
            .modified(Timestamp.valueOf(LocalDateTime.now())).build()
        );
    SubjectGroup subjectGroup = this.subjectGroupRepository
        .save(
            new SubjectGroup(TEST_GROUP_NAME, true, false, Timestamp.valueOf(LocalDateTime.now())));
    UserSubjectGroup userSubjectGroup = this.groupService.addUserToGroup(user, subjectGroup, true);
    assertThat(userSubjectGroup.getUser()).isEqualTo(user);
    assertThat(userSubjectGroup.getSubjectGroup()).isEqualTo(subjectGroup);
  }

  @Test(expected = RuntimeException.class)
  public void addUserToGroupUnknownUser() {
    User user = User.builder()
        .email("email@email.com")
        .mqttPassword("123456")
        .active(true)
        .created(Timestamp.valueOf(LocalDateTime.now()))
        .modified(Timestamp.valueOf(LocalDateTime.now())).build();
    SubjectGroup subjectGroup = this.subjectGroupRepository
        .save(
            new SubjectGroup(TEST_GROUP_NAME, true, false, Timestamp.valueOf(LocalDateTime.now())));
    this.groupService.addUserToGroup(user, subjectGroup, true);
  }

  @Test(expected = RuntimeException.class)
  public void addUserToGroupUnknownGroup() {
    User user = this.userRepository.save(
        User.builder()
            .email("email@email.com")
            .mqttPassword("123456")
            .active(true)
            .created(Timestamp.valueOf(LocalDateTime.now()))
            .modified(Timestamp.valueOf(LocalDateTime.now())).build()
    );
    SubjectGroup subjectGroup = new SubjectGroup(TEST_GROUP_NAME, true, false,
        Timestamp.valueOf(LocalDateTime.now()));
    this.groupService.addUserToGroup(user, subjectGroup, true);
  }

  @Test
  public void deleteExpiredGroupTest() throws Exception {
    PairingResponse pairingResponse = pairingCodeService.generatePairingCode(null, true);

    User user = userRepository.findOneByEmail(pairingResponse.getMqttUsername())
        .orElseThrow(NotFoundException::new);

    SubjectGroup subjectGroup = subjectGroupRepository.findById(pairingResponse.getGroupId())
        .orElseThrow(NotFoundException::new);
    subjectGroup.setModified(Timestamp.valueOf(LocalDateTime.now().minusDays(GroupService.EXPIRATION_DAYS)));
    subjectGroupRepository.save(subjectGroup);

    assertTrue(subjectGroupRepository.exists(pairingResponse.getGroupId()));
    assertTrue(subjectRepository.exists(pairingResponse.getSubjectId()));
    assertTrue(userRepository.exists(user.getId()));

    groupService.deleteExpiredGroup();

    assertFalse(subjectGroupRepository.exists(pairingResponse.getGroupId()));
    assertFalse(subjectRepository.exists(pairingResponse.getSubjectId()));
    assertFalse(userRepository.exists(user.getId()));
  }

  @Test
  public void getPrimaryUserTest() throws Exception {
    User user = this.userRepository
        .save(User.builder().email("email@email.com").mqttPassword("123456")
            .active(true)
            .created(Timestamp.valueOf(LocalDateTime.now()))
            .modified(Timestamp.valueOf(LocalDateTime.now())).build()
        );
    User user2 = this.userRepository
        .save(User.builder().email("email@email2.com").mqttPassword("1234567")
            .active(true)
            .created(Timestamp.valueOf(LocalDateTime.now()))
            .modified(Timestamp.valueOf(LocalDateTime.now())).build()
        );
    SubjectGroup subjectGroup = this.subjectGroupRepository
        .save(
            new SubjectGroup(TEST_GROUP_NAME, true, false, Timestamp.valueOf(LocalDateTime.now())));
    this.groupService.addUserToGroup(user, subjectGroup, true);
    this.groupService.addUserToGroup(user2, subjectGroup, false);

    User primaryUser = this.groupService.getPrimaryUser(subjectGroup.getId());

    assertThat(primaryUser.getEmail()).isEqualTo(user.getEmail());
    assertThat(primaryUser.getMqttPassword()).isEqualTo(user.getMqttPassword());
  }
}