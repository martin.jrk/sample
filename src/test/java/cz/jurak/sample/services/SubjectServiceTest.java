package cz.jurak.sample.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cz.jurak.sample.models.requests.GroupRequest;
import cz.jurak.sample.models.requests.SubjectRequest;
import cz.jurak.sample.models.responses.GroupResponse;
import cz.jurak.sample.models.responses.SubjectResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubjectServiceTest {

  @Autowired
  private SubjectService subjectService;

  @Autowired
  private GroupService groupService;

  @Test
  public void createSubjectTestOK() throws Exception {
    GroupRequest groupRequest = new GroupRequest("group", false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    SubjectRequest subjectRequest = new SubjectRequest("subject");
    SubjectResponse subjectResponse = this.subjectService.createSubject(groupResponse.getId(), subjectRequest);
    assertTrue(subjectResponse != null);
    assertTrue(subjectRequest.getName().equals(subjectResponse.getName()));
    assertTrue(subjectResponse.getId() != null);
  }

  @Test(expected = NotFoundException.class)
  public void createSubjectTestError() throws Exception {
    SubjectRequest subjectRequest = new SubjectRequest("subject");
    this.subjectService.createSubject(Long.MAX_VALUE, subjectRequest);
  }

  @Test
  public void updateSubjectTest() throws Exception {
    GroupRequest groupRequest = new GroupRequest("group", false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    SubjectRequest subjectRequestNew = new SubjectRequest("subject");
    SubjectRequest subjectRequestUpdate = new SubjectRequest("subject1");
    SubjectResponse subjectResponseNew = this.subjectService
        .createSubject(groupResponse.getId(), subjectRequestNew);
    SubjectResponse subjectResponseUpdate = this.subjectService
        .updateSubject(subjectResponseNew.getId(), subjectRequestUpdate);
    assertTrue(subjectResponseUpdate != null);
    assertFalse(subjectResponseUpdate.getName().equals(subjectResponseNew.getName()));
    assertTrue(subjectResponseUpdate.getId().equals(subjectResponseNew.getId()));
  }

  @Test
  public void deleteSubjectTestOK() throws Exception {
    GroupRequest groupRequest = new GroupRequest("group", false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    SubjectRequest subjectRequestNew = new SubjectRequest("subject");
    SubjectResponse subjectResponseNew = this.subjectService
        .createSubject(groupResponse.getId(), subjectRequestNew);
    this.subjectService.deleteSubject(subjectResponseNew.getId());
  }

  @Test(expected = Exception.class)
  public void deleteSubjectTestNotExist() throws Exception {
    GroupRequest groupRequest = new GroupRequest("group", false);
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    SubjectRequest subjectRequestNew = new SubjectRequest("subject");
    SubjectResponse subjectResponseNew = this.subjectService
        .createSubject(groupResponse.getId(), subjectRequestNew);
    this.subjectService.deleteSubject(subjectResponseNew.getId() + 1);
  }
}