package cz.jurak.sample.utils;

import java.util.Random;

public class CodeGenerator {

  public static String generateCode() {
    return formatNumber(new Random().nextInt(10000));
  }

  public static String formatNumber(int number) {
    return String.format("%04d", number);
  }
}
