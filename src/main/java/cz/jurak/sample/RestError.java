package cz.jurak.sample;

import cz.jurak.sample.models.responses.ErrorResponse;
import org.springframework.http.HttpStatus;

public enum RestError {

  OK(HttpStatus.OK, 0, "Success"),
  NOT_FOUND(HttpStatus.INTERNAL_SERVER_ERROR, 404, "Not found"),
  INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, 500, "Internal server error");

  private HttpStatus status;
  private Integer code;
  private String message;

  RestError(HttpStatus status, int code, String message) {
    this.status = status;
    this.code = code;
    this.message = message;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public ErrorResponse getErrorResponse() {
    return ErrorResponse.builder().status(this.status.value()).message(this.message).build();
  }
}
