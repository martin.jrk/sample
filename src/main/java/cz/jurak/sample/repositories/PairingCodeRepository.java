package cz.jurak.sample.repositories;

import cz.jurak.sample.models.dao.PairingCode;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface PairingCodeRepository extends GeneralRepository<PairingCode, Long> {

  Optional<PairingCode> findByCode(String code);

  List<PairingCode> findByExpireAtBefore(Date date);

  @Modifying
  @Query("delete from PairingCode pc where pc.code = ?1")
  void deletePairingCodeByCode(String pairingCode);
}
