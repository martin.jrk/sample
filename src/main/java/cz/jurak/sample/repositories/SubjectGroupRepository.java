package cz.jurak.sample.repositories;

import cz.jurak.sample.models.dao.SubjectGroup;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

public interface SubjectGroupRepository extends GeneralRepository<SubjectGroup, Long> {

  @Query("select sg.id, u.id from SubjectGroup sg "
      + "inner join sg.userSubjectGroups usg "
      + "inner join usg.user u "
      + "where usg.primary = true and u.trial = true and sg.modified <= ?1")
  List<Object[]> findExpiredGroupIds(Date expirationDate);
}