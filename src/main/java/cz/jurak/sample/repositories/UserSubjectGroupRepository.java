package cz.jurak.sample.repositories;

import cz.jurak.sample.models.dao.UserSubjectGroup;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;

public interface UserSubjectGroupRepository extends GeneralRepository<UserSubjectGroup, Long> {

  @Query("select usg from UserSubjectGroup usg where usg.subjectGroup.id = ?1 and usg.primary=true")
  Optional<UserSubjectGroup> findPrimaryUserByGroupId(Long groupId);
}
