package cz.jurak.sample.repositories;

import cz.jurak.sample.models.dao.Subject;
import cz.jurak.sample.models.dao.SubjectGroup;
import java.util.List;


public interface SubjectRepository extends GeneralRepository<Subject, Long> {

  List<Subject> findBySubjectGroup(SubjectGroup subjectGroup);
}
