package cz.jurak.sample.repositories;

import cz.jurak.sample.models.dao.SubjectGroup;
import cz.jurak.sample.models.dao.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;


public interface UserRepository extends GeneralRepository<User, Long> {

  Optional<User> findOneByEmail(String email);

  Optional<User> findOneByEmailAndMqttPassword(String email, String mqttPassword);

  @Query("select usg.user from UserSubjectGroup usg where usg.subjectGroup = ?1")
  List<User> findUsersBySubjectGroup(SubjectGroup subjectGroup);

  @Query("select usg.user from UserSubjectGroup usg where usg.subjectGroup = ?1 and usg.primary = true")
  Optional<User> findPrimaryUserInSubjectGroup(SubjectGroup subjectGroup);
}
