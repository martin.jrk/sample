package cz.jurak.sample.models.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "GroupRequest")
public class GroupRequest {

  @NotNull
  @ApiModelProperty(notes = "Group name", required = true, example = "My family")
  private String name;

  @NotNull
  @ApiModelProperty(notes = "True if group is for test purpose", required = true, example = "false")
  private Boolean test;
}
