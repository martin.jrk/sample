package cz.jurak.sample.models.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SubjectRequest")
public class SubjectRequest {

  @NotNull
  @ApiModelProperty(notes = "Subject name", required = true, example = "Dite")
  private String name;
}
