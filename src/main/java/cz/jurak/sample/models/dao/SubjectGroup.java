package cz.jurak.sample.models.dao;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(exclude = {"userSubjectGroups", "pairingCodes", "subjects"})

@Entity
@Table(name = "subject_group")
public class SubjectGroup implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "id", unique = true, nullable = false)
  private Long id;

  @NonNull
  @Column(name = "name", nullable = false)
  private String name;

  @NonNull
  @Column(name = "active", nullable = false)
  private boolean active;

  @NonNull
  @Column(name = "test", nullable = false)
  private boolean test;

  @NonNull
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created", nullable = false, length = 19)
  private Date created;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modified", nullable = false, length = 19)
  private Date modified;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "deactivated", length = 19)
  private Date deactivated;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "subjectGroup", cascade = CascadeType.REMOVE)
  private Set<UserSubjectGroup> userSubjectGroups = new HashSet<UserSubjectGroup>(0);

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "subjectGroup", cascade = CascadeType.REMOVE)
  private Set<PairingCode> pairingCodes = new HashSet<PairingCode>(0);

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "subjectGroup", cascade = CascadeType.REMOVE)
  private Set<Subject> subjects = new HashSet<Subject>(0);

}
