package cz.jurak.sample.models.dao;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor

@Entity
@Table(name = "subject")
public class Subject implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "id", unique = true, nullable = false)
  private Long id;

  @NonNull
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "subject_group_id", nullable = false)
  private SubjectGroup subjectGroup;

  @NonNull
  @Column(name = "name", nullable = false)
  private String name;

  @NonNull
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created", nullable = false, length = 19)
  private Date created;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modified", nullable = false, length = 19)
  private Date modified;
}
