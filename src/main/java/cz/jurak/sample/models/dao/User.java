package cz.jurak.sample.models.dao;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "user")
public class User implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "id", unique = true, nullable = false)
  private Long id;

  @Column(name = "email", nullable = false)
  private String email;

  @Column(name = "active", nullable = false)
  private boolean active;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created", nullable = false, length = 19)
  private Date created;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "deactivated", length = 19)
  private Date deactivated;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modified", nullable = false, length = 19)
  private Date modified;

  @Column(name = "mqtt_password", nullable = false)
  private String mqttPassword;

  @Column(name = "trial", nullable = false)
  private boolean trial;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.REMOVE)
  private Set<UserSubjectGroup> userSubjectGroups = new HashSet<UserSubjectGroup>(0);
}
