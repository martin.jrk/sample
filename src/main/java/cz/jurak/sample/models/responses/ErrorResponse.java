package cz.jurak.sample.models.responses;

import io.swagger.annotations.ApiModel;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(value = "ErrorResponse")
public class ErrorResponse {

  private LocalDate timestamp;
  private Integer status;
  private String error;
  private String exception;
  private String message;
  private String path;
}
