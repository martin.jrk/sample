package cz.jurak.sample.models.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SubjectResponse")
public class SubjectResponse {

  @ApiModelProperty(notes = "Subject id", required = true, example = "1")
  private Long id;
  @ApiModelProperty(notes = "Subject name", required = true, example = "Dite")
  private String name;
}
