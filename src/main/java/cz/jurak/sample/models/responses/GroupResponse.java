package cz.jurak.sample.models.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "GroupResponse")
public class GroupResponse {

  @ApiModelProperty(notes = "Group id", required = true, example = "1")
  private Long id;
  @ApiModelProperty(notes = "Group name", required = true, example = "My family")
  private String name;
  @ApiModelProperty(notes = "True if group is for test purposes", required = true, example = "true")
  private boolean test;
}
