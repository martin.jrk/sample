package cz.jurak.sample.models.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "UserResponse")
public class UserResponse {

  @ApiModelProperty(notes = "User name", required = true, example = "john doe")
  private String name;
  @ApiModelProperty(notes = "Email", required = true, example = "johndoe@email.com")
  private String email;
  @ApiModelProperty(notes = "True if user is for test purposes", required = true, example = "true")
  private boolean test;
}
