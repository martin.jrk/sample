package cz.jurak.sample.models.responses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "PairingResponse")
public class PairingResponse {

  @ApiModelProperty(notes = "Group id", required = true, example = "1")
  private Long groupId;
  @ApiModelProperty(notes = "Pairing key", required = true, example = "1234")
  private String code;
  @ApiModelProperty(notes = "Subject id", example = "1")
  private Long subjectId;
  @ApiModelProperty(notes = "Mqtt username", example = "email@email.com")
  private String mqttUsername;
  @ApiModelProperty(notes = "Mqtt password", example = "abcdefghijklmn")
  private String mqttPassword;
}
