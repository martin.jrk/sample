package cz.jurak.sample;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
class GlobalExceptionHandler {

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler({NotFoundException.class, EmptyResultDataAccessException.class})
  public void handleNotFoundException(HttpServletResponse response, Exception e) throws IOException {
    log.error("Exception", e);
    response.sendError(HttpStatus.NOT_FOUND.value());
  }

  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  @ExceptionHandler(BadCredentialsException.class)
  public void handleUnauthorizedException(HttpServletResponse response, Exception e) throws IOException {
    log.error("Exception", e);
    response.sendError(HttpStatus.UNAUTHORIZED.value());
  }

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public void handleFcmErrorException(HttpServletResponse response, Exception e) throws IOException {
    log.error("Exception", e);
    response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
  }
}
