package cz.jurak.sample.scheduled_tasks;

import cz.jurak.sample.services.GroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GroupScheduler {

  @Autowired
  private GroupService groupService;

  //once a day at 0:00
  @Scheduled(cron = "0 0 0 * * *")
  public void deleteExpiredTrialGroup() {
    log.info("START Delete expiration trial group");
    groupService.deleteExpiredGroup();
    log.info("END Delete expiration trial group");

  }
}