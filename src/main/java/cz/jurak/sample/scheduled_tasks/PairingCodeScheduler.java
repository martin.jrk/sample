package cz.jurak.sample.scheduled_tasks;

import cz.jurak.sample.services.PairingCodeService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PairingCodeScheduler {

  @Autowired
  private PairingCodeService pairingCodeService;

  @Scheduled(fixedDelay = 600000)
  public void deleteExpiredPairingCodes() throws Exception {
    log.info("START Delete pairing codes");
    this.pairingCodeService.deleteExpiredPairingCodes();
    log.info("END Delete pairing codes");
  }
}
