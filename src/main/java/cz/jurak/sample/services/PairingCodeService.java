package cz.jurak.sample.services;

import cz.jurak.sample.models.dao.PairingCode;
import cz.jurak.sample.models.dao.SubjectGroup;
import cz.jurak.sample.models.dao.User;
import cz.jurak.sample.models.responses.PairingResponse;
import cz.jurak.sample.repositories.PairingCodeRepository;
import cz.jurak.sample.repositories.SubjectGroupRepository;
import cz.jurak.sample.repositories.UserRepository;
import cz.jurak.sample.utils.CodeGenerator;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PairingCodeService {

  private static final int EXPIRATION_TIME = 5;

  @Autowired
  private PairingCodeRepository pairingCodeRepository;

  @Autowired
  private SubjectGroupRepository subjectGroupRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private SubjectService subjectService;

  @Autowired
  private UserService userService;

  @Autowired
  private GroupService groupService;

  @Autowired
  private MapperFacade mapperFacade;

  /**
   * Method generate pairing code and relate it to group. If group id is set, code is related to existing group.
   *
   * @param groupId group id
   * @param createSubject create default subject
   * @return pairing response
   * @throws NotFoundException group not found
   * @
   */
  public PairingResponse generatePairingCode(Long groupId, boolean createSubject) throws NotFoundException {
    SubjectGroup subjectGroup;
    User user;
    if (groupId == null) {
      subjectGroup = this.groupService.createAnonymousGroup();
      user = this.userService.createTrialUser();
      this.groupService.addUserToGroup(user, subjectGroup, true);
    } else {
      subjectGroup = this.subjectGroupRepository.findById(groupId).orElseThrow(NotFoundException::new);
      user = subjectGroup.getUserSubjectGroups().iterator().next().getUser();
    }

    PairingCode pairingCode = this.setExpirationTimeAndSavePairingCode(
        PairingCode.builder()
            .subjectGroup(subjectGroup)
            .code(CodeGenerator.generateCode())
            .build()
    );

    PairingResponse pairingResponse = mapperFacade.map(pairingCode, PairingResponse.class);
    pairingResponse.setMqttUsername(user.getEmail());
    pairingResponse.setMqttPassword(user.getMqttPassword());
    if (createSubject) {
      pairingResponse.setSubjectId(this.subjectService.createAnonymousSubject(subjectGroup).getId());
    }

    return pairingResponse;
  }

  /**
   * Confirm pairing code and related group.
   *
   * @param code pairing code
   * @return pairing response
   * @throws IllegalArgumentException wrong pairing code
   */
  public PairingResponse confirmPairingCode(String code) throws NotFoundException {
    PairingCode pairingCode = this.pairingCodeRepository.findByCode(code).orElseThrow(NotFoundException::new);
    User user = this.userRepository.findPrimaryUserInSubjectGroup(pairingCode.getSubjectGroup())
        .orElseThrow(NotFoundException::new);

    this.setExpirationTimeAndSavePairingCode(pairingCode);
    PairingResponse pairingResponse = mapperFacade.map(pairingCode, PairingResponse.class);
    pairingResponse.setMqttPassword(user.getMqttPassword());
    pairingResponse.setMqttUsername(user.getEmail());
    return pairingResponse;
  }

  /**
   * Delete expired pairing codes.
   */
  public void deleteExpiredPairingCodes() {
    List<PairingCode> pairingCodes = this.pairingCodeRepository
        .findByExpireAtBefore(Timestamp.valueOf(LocalDateTime.now()));
    this.pairingCodeRepository.delete(pairingCodes);
  }

  /**
   * Set expire time and save pairing code
   *
   * @param pairingCode pairing code
   * @return pairing code
   */
  private PairingCode setExpirationTimeAndSavePairingCode(PairingCode pairingCode) {
    pairingCode.setExpireAt(Timestamp.valueOf(LocalDateTime.now().plusMinutes(EXPIRATION_TIME)));
    return this.pairingCodeRepository.save(pairingCode);
  }

  /**
   * Delete pairing code
   */
  public void deletePairingCode(String pairingCode) {
    this.pairingCodeRepository.deletePairingCodeByCode(pairingCode);
  }

}
