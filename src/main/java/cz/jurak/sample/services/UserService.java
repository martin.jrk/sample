package cz.jurak.sample.services;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

import cz.jurak.sample.models.dao.User;
import cz.jurak.sample.models.responses.UserResponse;
import cz.jurak.sample.repositories.UserRepository;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService {

  private static final String TRIAL_EMAIL = "@trial-user.trial";

  @Autowired
  private UserRepository userRepository;

  public UserResponse getUser(String email) throws NotFoundException {
    User user = this.userRepository.findOneByEmail(email).orElseThrow(NotFoundException::new);
    return new UserResponse(user.getEmail(), user.getEmail(), true);
  }

  public User createTrialUser() {
    String email = this.generateEmail();
    String mqttPassword = this.generatePassword();
    User newUser = User.builder()
        .email(email)
        .mqttPassword(mqttPassword)
        .active(true)
        .created(Timestamp.valueOf(LocalDateTime.now()))
        .modified(Timestamp.valueOf(LocalDateTime.now()))
        .trial(true)
        .build();
    return this.userRepository.save(newUser);
  }

  public UserResponse getUserByEmailAndMqttPassword(String email, String mqttPassword) throws NotFoundException {
    User user = this.userRepository.findOneByEmailAndMqttPassword(email, mqttPassword)
        .orElseThrow(NotFoundException::new);
    return new UserResponse(user.getEmail(), user.getEmail(), true);
  }

  public UserResponse saveUser(String email) throws NotFoundException {
    User newUser = User.builder()
        .email(email)
        .mqttPassword(this.generatePassword())
        .active(true)
        .created(Timestamp.valueOf(LocalDateTime.now()))
        .modified(Timestamp.valueOf(LocalDateTime.now()))
        .build();
    User user = this.userRepository.save(newUser);
    return new UserResponse(user.getEmail(), user.getEmail(), true);
  }

  private String generateEmail() {
    String uuid = UUID.randomUUID().toString();
    return uuid + TRIAL_EMAIL;
  }

  private String generatePassword() {
    return new RandomStringGenerator.Builder().withinRange('0', 'z').filteredBy(LETTERS, DIGITS).build().generate(32);
  }

}
