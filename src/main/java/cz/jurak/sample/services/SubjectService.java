package cz.jurak.sample.services;

import cz.jurak.sample.models.dao.Subject;
import cz.jurak.sample.models.dao.SubjectGroup;
import cz.jurak.sample.models.requests.SubjectRequest;
import cz.jurak.sample.models.responses.SubjectResponse;
import cz.jurak.sample.repositories.SubjectGroupRepository;
import cz.jurak.sample.repositories.SubjectRepository;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SubjectService {

  private static final String DEFAULT_NAME = "default";

  @Autowired
  private SubjectRepository subjectRepository;

  @Autowired
  private SubjectGroupRepository subjectGroupRepository;

  @Autowired
  private MapperFacade mapperFacade;

  /**
   * Create a subject in group
   * @param groupId
   * @param subjectRequest
   * @return
   * @throws NotFoundException group not found
   */
  public SubjectResponse createSubject(Long groupId, SubjectRequest subjectRequest)
      throws NotFoundException {
    SubjectGroup subjectGroup = this.subjectGroupRepository.findById(groupId).orElseThrow(NotFoundException::new);

    return mapperFacade.map(
        this.subjectRepository.save(
            new Subject(subjectGroup, subjectRequest.getName(), Timestamp.valueOf(LocalDateTime.now()))),
        SubjectResponse.class);
  }

  public SubjectResponse updateSubject(Long subjectId, SubjectRequest subjectRequest) throws NotFoundException {
    Subject subject = this.subjectRepository.findById(subjectId).orElseThrow(NotFoundException::new);
    subject.setName(subjectRequest.getName());
    return mapperFacade.map(this.subjectRepository.save(subject), SubjectResponse.class);
  }

  /**
   * Get subjects from group
   * @param groupId
   * @return subjects
   * @throws NotFoundException group not found
   */
  public List<SubjectResponse> getSubjects(Long groupId) throws NotFoundException {
    SubjectGroup subjectGroup = this.subjectGroupRepository.findById(groupId).orElseThrow(NotFoundException::new);
    return mapperFacade.mapAsList(this.subjectRepository.findBySubjectGroup(subjectGroup), SubjectResponse.class);
  }

  public void deleteSubject(Long subjectId) {
    this.subjectRepository.delete(subjectId);
  }

  public Subject createAnonymousSubject(SubjectGroup subjectGroup) throws NotFoundException {
    return this.subjectRepository
        .save(new Subject(subjectGroup, DEFAULT_NAME, Timestamp.valueOf(LocalDateTime.now())));
  }
}
