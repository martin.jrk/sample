package cz.jurak.sample.services;

import cz.jurak.sample.models.dao.SubjectGroup;
import cz.jurak.sample.models.dao.User;
import cz.jurak.sample.models.dao.UserSubjectGroup;
import cz.jurak.sample.models.requests.GroupRequest;
import cz.jurak.sample.models.responses.GroupResponse;
import cz.jurak.sample.repositories.SubjectGroupRepository;
import cz.jurak.sample.repositories.UserRepository;
import cz.jurak.sample.repositories.UserSubjectGroupRepository;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GroupService {

  public static final long EXPIRATION_DAYS = 30;
  private static final String DEFAULT_NAME = "default";

  @Autowired
  private SubjectGroupRepository subjectGroupRepository;

  @Autowired
  private UserSubjectGroupRepository userSubjectGroupRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private MapperFacade mapperFacade;

  public GroupResponse createGroup(GroupRequest groupRequest) {
    SubjectGroup subjectGroup = mapperFacade.map(groupRequest, SubjectGroup.class);
    subjectGroup.setActive(true);
    subjectGroup.setCreated(Timestamp.valueOf(LocalDateTime.now()));
    return mapperFacade.map(this.subjectGroupRepository.save(subjectGroup), GroupResponse.class);
  }

  public GroupResponse getGroup(Long groupId) throws NotFoundException {
    SubjectGroup subjectGroup = this.subjectGroupRepository.findById(groupId).orElseThrow(NotFoundException::new);
    return mapperFacade.map(subjectGroup, GroupResponse.class);
  }

  public SubjectGroup createAnonymousGroup() {
    return this.subjectGroupRepository
        .save(new SubjectGroup(DEFAULT_NAME, true, false, Timestamp.valueOf(LocalDateTime.now())));
  }

  /**
   * Add user to group
   *
   * @param user
   * @param subjectGroup group
   * @param primary true if user is primary (group admin)
   * @return
   */
  public UserSubjectGroup addUserToGroup(User user, SubjectGroup subjectGroup, boolean primary) {
    UserSubjectGroup userSubjectGroup = UserSubjectGroup.builder().user(user)
        .subjectGroup(subjectGroup)
        .primary(primary).build();
    return this.userSubjectGroupRepository.save(userSubjectGroup);
  }

  /**
   * Delete all expired groups and their users
   */
  public void deleteExpiredGroup() {
    LocalDateTime expirationDate = LocalDateTime.now().minusDays(EXPIRATION_DAYS);
    Set<Long> groupIds = new HashSet<>();
    Set<Long> userIds = new HashSet<>();
    List<Object[]> expiredGroups = subjectGroupRepository.findExpiredGroupIds(Timestamp.valueOf(expirationDate));
    expiredGroups.forEach(e -> {
      groupIds.add((Long) e[0]);
      userIds.add((Long) e[1]);
    });
    userIds.forEach(e -> userRepository.delete(e));
    groupIds.forEach(e -> subjectGroupRepository.delete(e));
  }

  /**
   * Get primary user of group
   * @param groupId
   * @return primary user
   * @throws NotFoundException group not found
   */
  public User getPrimaryUser(Long groupId) throws NotFoundException {
    return this.userSubjectGroupRepository.findPrimaryUserByGroupId(groupId).orElseThrow(NotFoundException::new)
        .getUser();

  }
}