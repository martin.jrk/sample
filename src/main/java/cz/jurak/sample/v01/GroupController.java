package cz.jurak.sample.v01;

import cz.jurak.sample.RestError;
import cz.jurak.sample.models.requests.GroupRequest;
import cz.jurak.sample.models.responses.ErrorResponse;
import cz.jurak.sample.models.responses.GroupResponse;
import cz.jurak.sample.services.GroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Api
@Slf4j
@RestController
@RequestMapping("/v1/groups")
public class GroupController {

  @Autowired
  private GroupService groupService;

  @ApiOperation(value = "Create group", nickname = "createGroup")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created", response = GroupResponse.class),
      @ApiResponse(code = 400, message = "Bad request", response = DefaultErrorAttributes.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [500]", response = ErrorResponse.class)
  })
  @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.CREATED)
  public @ResponseBody
  GroupResponse createGroup(@Valid @RequestBody GroupRequest groupRequest) {
    log.info("REST POST /v1/groups START");
    GroupResponse groupResponse = this.groupService.createGroup(groupRequest);
    log.info("REST POST /v1/groups END");
    return groupResponse;
  }

  @ApiOperation(value = "Get group by id", nickname = "getGroup")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success", response = GroupResponse.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [404, 500]", response = ErrorResponse.class)
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "{groupId}")
  @ResponseStatus(value = HttpStatus.OK)
  public @ResponseBody
  GroupResponse getGroup(
      @ApiParam(value = "ID of group that needs to be fetched", defaultValue = "1", required = true) @PathVariable("groupId") Long groupId)
      throws Exception {
    log.info("REST GET /v1/groups START");
    GroupResponse groupResponse = this.groupService.getGroup(groupId);
    log.info("REST GET /v1/groups END");
    return groupResponse;
  }
}
