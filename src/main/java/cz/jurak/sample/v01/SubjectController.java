package cz.jurak.sample.v01;

import cz.jurak.sample.RestError;
import cz.jurak.sample.models.requests.SubjectRequest;
import cz.jurak.sample.models.responses.ErrorResponse;
import cz.jurak.sample.models.responses.SubjectResponse;
import cz.jurak.sample.services.SubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Api
@Slf4j
@RestController
@RequestMapping("/v1/groups/{groupId}/subjects")
public class SubjectController {

  @Autowired
  private SubjectService subjectService;

  @ApiOperation(value = "Get subjects of group",
      nickname = "getSubjects")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success", response = SubjectResponse.class, responseContainer = "List"),
      @ApiResponse(code = 500, message = "Internal server error - errors [500]", response = ErrorResponse.class)
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public @ResponseBody
  List<SubjectResponse> getSubjects(
      @ApiParam(value = "ID of group that needs to be fetched", defaultValue = "1", required = true) @PathVariable("groupId") Long groupId)
      throws Exception {
    log.info("REST GET /v1/subjects START");
    List<SubjectResponse> subjectResponses = this.subjectService.getSubjects(groupId);
    log.info("REST GET /v1/subjects END");
    return subjectResponses;
  }

  @ApiOperation(value = "Create subject",
      nickname = "createSubject")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created", response = SubjectResponse.class),
      @ApiResponse(code = 400, message = "Bad request", response = DefaultErrorAttributes.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [404, 500]", response = ErrorResponse.class)
  })
  @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.CREATED)
  public @ResponseBody
  SubjectResponse createSubject(
      @ApiParam(value = "ID of group that needs to be fetched", defaultValue = "4", required = true) @PathVariable("groupId") Long groupId,
      @Valid @RequestBody SubjectRequest subjectRequest)
      throws Exception {
    log.info("REST POST /v1/groups/{groupId}/subjects START");
    SubjectResponse subjectResponse = this.subjectService.createSubject(groupId, subjectRequest);
    log.info("REST POST /v1/groups/{groupId}/subjects END");
    return subjectResponse;
  }

  @ApiOperation(value = "Update subject",
      nickname = "updateSubject")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created", response = SubjectResponse.class),
      @ApiResponse(code = 400, message = "Bad request", response = DefaultErrorAttributes.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [404, 500]", response = ErrorResponse.class)
  })
  @PutMapping(path = "{subjectId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.CREATED)
  public @ResponseBody
  SubjectResponse updateSubject(
      @ApiParam(value = "ID of group that needs to be fetched", defaultValue = "1", required = true) @PathVariable("groupId") Long groupId,
      @ApiParam(value = "ID of subject that needs to be fetched", defaultValue = "1", required = true) @PathVariable("subjectId") Long subjectId,
      @Valid @RequestBody SubjectRequest subjectRequest)
      throws Exception {
    log.info("REST POST /v1/groups/{groupId}/subjects/{subjectId} START");
    SubjectResponse subjectResponse = this.subjectService.updateSubject(subjectId, subjectRequest);
    log.info("REST POST /v1/groups/{groupId}/subjects/{subjectId} END");
    return subjectResponse;
  }

  @ApiOperation(value = "Delete subject",
      nickname = "deleteSubject")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "OK", response = ErrorResponse.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [500]", response = ErrorResponse.class)
  })
  @DeleteMapping(path = "{subjectId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public ErrorResponse deleteSubject(
      @ApiParam(value = "ID of group that needs to be fetched", defaultValue = "1", required = true) @PathVariable("groupId") Long groupId,
      @ApiParam(value = "ID of subject that needs to be fetched", defaultValue = "1", required = true) @PathVariable("subjectId") Long subjectId) {
    log.info("REST DELETE /v1/groups/{groupId}/subjects/{subjectId} START");
    this.subjectService.deleteSubject(subjectId);
    log.info("REST DELETE /v1/groups/{groupId}/subjects/{subjectId} END");
    return RestError.OK.getErrorResponse();
  }
}