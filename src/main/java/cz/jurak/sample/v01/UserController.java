package cz.jurak.sample.v01;

import cz.jurak.sample.models.responses.ErrorResponse;
import cz.jurak.sample.models.responses.UserResponse;
import cz.jurak.sample.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.security.Principal;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Api
@Slf4j
@RestController
@RequestMapping("/v1/users")
public class UserController {
  
  @Autowired
  private UserService userService;

  @ApiOperation(value = "Save user", nickname = "saveUser",
      authorizations = {
          @Authorization(
              value = "Authorization"
          )
      })
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created", response = UserResponse.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [404, 500]", response = ErrorResponse.class)
  })
  @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public UserResponse saveUser(Principal principal) throws Exception {
    log.info("REST POST /v1/users START");
    UserResponse userResponse = userService.saveUser(principal.getName());
    log.info("REST POST /v1/users END");
    return userResponse;
  }

  @ApiOperation(value = "Get user or verify if user exists", nickname = "getUser",
      authorizations = {
          @Authorization(
              value = "Authorization"
          )
      })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success", response = UserResponse.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [404, 500]", response = ErrorResponse.class)
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  public UserResponse getUser(Principal principal) throws Exception {
    log.info("REST GET /v1/users START");
    UserResponse userResponse = userService.getUser(principal.getName());
    log.info("REST GET /v1/users END");
    return userResponse;
  }
}
