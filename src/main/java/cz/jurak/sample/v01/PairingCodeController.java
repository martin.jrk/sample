package cz.jurak.sample.v01;

import cz.jurak.sample.RestError;
import cz.jurak.sample.models.responses.ErrorResponse;
import cz.jurak.sample.models.responses.PairingResponse;
import cz.jurak.sample.services.PairingCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api
@RestController
@RequestMapping("/v1/pairing_codes")
public class PairingCodeController {

  @Autowired
  private PairingCodeService pairingCodeService;

  @ApiOperation(value = "Generate pairing code", nickname = "generatePairingCode")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Created", response = PairingResponse.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [500]", response = ErrorResponse.class)
  })
  @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.CREATED)
  @ResponseBody
  public PairingResponse generatePairingCode(
      @ApiParam(value = "Group ID") @RequestParam(name = "groupId", required = false) Long groupId,
      @ApiParam(value = "Create default subject for group") @RequestParam(name = "createSubject", required = false) boolean createSubject)
      throws Exception {
    log.info("START REST POST /v1/pairing_codes groupId={}, createSubject={}", groupId, createSubject);
    PairingResponse pairingResponse = this.pairingCodeService.generatePairingCode(groupId, createSubject);
    log.info("END REST POST /v1/pairing_codes {}", pairingResponse);
    return pairingResponse;
  }


  @ApiOperation(value = "Confirm pairing code", nickname = "confirmPairingCode")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success", response = PairingResponse.class),
      @ApiResponse(code = 404, message = "Not found", response = ErrorResponse.class),
      @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
  })
  @PostMapping(path = "/confirm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  @ResponseBody
  public PairingResponse confirmPairingCode(
      @ApiParam(value = "Pairing code", defaultValue = "1234", required = true) @RequestParam("pairingCode") String pairingCode)
      throws Exception {
    log.info("START REST POST /v1/pairing_codes pairingCode={} START", pairingCode);
    PairingResponse pairingResponse = this.pairingCodeService.confirmPairingCode(pairingCode);
    log.info("END REST POST /v1/pairing_codes ");
    return pairingResponse;
  }

  @ApiOperation(value = "Delete pairing code", nickname = "deletePairingCode")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success", response = ErrorResponse.class),
      @ApiResponse(code = 500, message = "Internal server error - errors [500]", response = ErrorResponse.class)
  })
  @DeleteMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(value = HttpStatus.OK)
  @ResponseBody
  public ErrorResponse deletePairingCode(
      @ApiParam(value = "Pairing code", defaultValue = "1234", required = true) @RequestParam("pairingCode") String pairingCode) {
    log.info("START REST DELETE /v1/pairing_codes pairingCode={}", pairingCode);
    this.pairingCodeService.deletePairingCode(pairingCode);
    log.info("END REST DELETE /v1/pairing_codes");
    return RestError.OK.getErrorResponse();
  }
}
