package cz.jurak.sample.configurations;

import io.swagger.annotations.Api;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

  @Bean
  public Docket swaggerSettings(final EndpointHandlerMapping actuatorEndpointHandlerMapping) {
    ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2).select();

    return builder.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
        .paths(PathSelectors.any())
        .build()

        .useDefaultResponseMessages(false);
  }

}
