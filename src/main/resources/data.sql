INSERT INTO `subject_group` (id, name, active, test, created, modified, deactivated) VALUES
  (1,'Group1',true,false,'2017-07-28 15:09:55','2017-07-28 13:09:55',NULL),
  (2,'Group2',true,false,'2017-07-28 15:14:05','2017-10-27 07:37:49',NULL),
  (3,'Group3',true,false,'2017-07-31 15:43:36','2017-07-31 13:43:36',NULL);

INSERT INTO `user` (id, email, active, created, deactivated, modified, mqtt_password, trial) VALUES
  (1,'user1@example.com',1,'2017-09-20 10:15:22',NULL,'2017-09-20 08:15:22','123',0),
  (2,'user2@example.com',1,'2017-10-17 09:20:17',NULL,'2017-10-17 07:20:17','123',0),
  (3,'user3@example.com',1,'2017-10-17 09:20:30',NULL,'2017-10-17 07:20:30','123',0);

INSERT INTO `user_subject_group` (id, subject_group_id, user_id, _primary) VALUES
  (1,1,1,1),
  (2,2,2,1),
  (3,3,3,1);

INSERT INTO `subject` (id, name, created, modified, subject_group_id) VALUES
  (1,'subject1','2017-07-31 16:49:44','2017-07-31 14:49:44',1),
  (2,'subject2','2017-08-02 14:01:15','2017-08-02 12:01:14',2),
  (3,'subject3','2017-08-07 15:54:41','2017-08-07 13:54:41',3)