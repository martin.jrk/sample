API:  
http://localhost:8080/sample/swagger-ui.html

Monitoring:  
http://localhost:8080/sample/monitoring

## Popis

Ukazkova serverova aplikace napsana v Java 8 a frameworku Spring boot. 

Technologie:
- h2 db 
- swagger
- lombok
- orika

## Spusteni

Spusteni lokalne `mvn clean install spring-boot:run`.


